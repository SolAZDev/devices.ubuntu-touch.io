module.exports = {
  runBefore: beforeElaboration,
  runAfter: afterElaboration
};

let Validator = require("./dataValidator.js");
const rules = require("../../data/validationRules.js")();

const portStatus = require("../../data/portStatus.json");
const progressStages = require("../../data/progressStages.json");

const dataUtils = require("./dataUtils.js");

const path = require("path");

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

function beforeElaboration(device) {
  let ciDiff = [];

  try {
    ciDiff = process.env.CI_FILES_CHANGED.split("\n").map((e) => {
      return path.basename(e, device.fileInfo.extension);
    });
  } catch (e) {}

  if (
    !process.argv.includes("test-changes") ||
    ciDiff.includes(device.fileInfo.name)
  ) {
    let validation = new Validator(device, rules);
    validation.passes();
    let notValid = validation.errors();

    console.log(
      notValid.length
        ? ansiCodes.yellowFG + "%s" + ansiCodes.reset
        : ansiCodes.greenFG + "%s" + ansiCodes.reset,
      "\n[" + device.fileInfo.name + "] " + device.name
    );

    notValid.forEach((err) => console.log(err));
    device.notValid = notValid;
  }
}

function afterElaboration(device) {
  if (device.portStatus) {
    nextStageRequirements(device);
  }
}

/* Functions */
function nextStageRequirements(device) {
  let missing = [];
  let nextStage = device.progressStage.number + 1;

  if (nextStage == progressStages.length) return;

  dataUtils.forEachFeature(device, function (feature, category) {
    if (feature.stage == nextStage) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);
      if (graphQlFeature && graphQlFeature.value != "+") {
        missing.push(category.categoryName + ": " + feature.name);
      }
    }
  });

  device.nextProgressStage = {
    number: nextStage,
    name: progressStages[nextStage].name,
    description: progressStages[nextStage].description
  };
  device.nextStageRequirements = missing;
}
